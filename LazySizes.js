import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import { empty } from '../../core/utils/helpers';

const canUseDOM = !!(
  typeof window !== 'undefined' &&
  window.document &&
  window.document.createElement
);

let lazySizes = null;

if (canUseDOM) {
  lazySizes = require('lazysizes');
  // lazySizes.cfg.expand = 1;
  lazySizes.cfg.expand = 300;
  require('lazysizes/plugins/bgset/ls.bgset.min.js');
}

class LazyLoad extends PureComponent {
  static propTypes = {
    src: PropTypes.string,
    dataSizes: PropTypes.string,
    dataSrc: PropTypes.string,
    dataSrcSet: PropTypes.oneOfType([PropTypes.string, PropTypes.object, PropTypes.array]),
    className: PropTypes.string,
    iframe: PropTypes.bool,
    dataBg: PropTypes.string,
    dataBgSet: PropTypes.string,
  };

  static defaultProps = {
    src: 'data:image/gif;base64,R0lGODdhEAAJAIAAAMLCwsLCwiwAAAAAEAAJAAACCoSPqcvtD6OclBUAOw==',
    dataSizes: 'auto',
    iframe: false,
    dataBg: null,
    dataBgSet: null,
  };

  getSnapshotBeforeUpdate = (prevProps) => {
    let propsChanged = false;
    for (let propName of ['src', 'dataSizes', 'dataSrc', 'dataSrcSet', 'className', 'iframe', 'dataBg', 'dataBgSet']) {
      let prop = propName === 'dataSrcSet' ? this.handleSrcSet(this.props[propName]) : this.props[propName];
      let nextProp = propName === 'dataSrcSet' ? this.handleSrcSet(prevProps[propName]) : prevProps[propName];
      if (prop !== nextProp) {
        propsChanged = true;
        break;
      }
    }
    if (propsChanged && lazySizes) {
      let lazyElement = ReactDOM.findDOMNode(this);
      if (lazySizes.hC(lazyElement, 'lazyloaded')) {
        lazySizes.rC(lazyElement, 'lazyloaded');
      }
    }

    return null;
  };

  componentDidUpdate = () => {
    if (!lazySizes) {
      return;
    }
    let lazyElement = ReactDOM.findDOMNode(this);
    if (!lazySizes.hC(lazyElement, 'lazyloaded') && !lazySizes.hC(lazyElement, 'lazyload')) {
      lazySizes.aC(lazyElement, 'lazyload');
    }
  };

  handleSrcSet = (srcSet) => {
    let result = srcSet;
    if (typeof srcSet === 'object') {
      if (!Array.isArray(srcSet)) {
        result = [];
        for (let variant in srcSet) {
          if (srcSet.hasOwnProperty(variant)) {
            result.push({
              variant: variant,
              src: srcSet[variant]
            });
          }
        }
      }
      result = result.map(item => {
        return `${item.src} ${item.variant}`;
      }).join(', ');
    }
    return result;
  };

  render() {
    let {
      src,
      dataSizes,
      dataSrc,
      dataSrcSet,
      iframe,
      dataBg,
      dataBgSet,
      className,
      ...other
    } = this.props;

    dataSrcSet = this.handleSrcSet(dataSrcSet);
    className = `${className} lazyload`;

    if (iframe) {
      return (
        <iframe
          {...other}
          src={dataSrc ? '' : src}
          data-src={dataSrc}
          className={className}
        />
      );
    } else if (!empty(dataBg) && !empty(dataBgSet)) {
      return (
        <div
          {...other}
          data-bg={dataBg}
          data-bgset={dataBgSet}
          data-sizes={dataSizes}
          className={`${className} ratio-container`}
        />
      )
    }
    return (
      <img
        {...other}
        src={src}
        data-src={dataSrc}
        data-sizes={dataSizes}
        data-srcset={dataSrcSet}
        className={className}
      />
    );
  }
}

export default LazyLoad;
