// helper utilities

// check if the object has a valid featured image
// if !medium-large size then uses source_url
export function hasFeaturedImage(obj) {
  switch (true) {
    case obj.featured_media === 0:
      return false;

    case !isNotTypeOfUndefined(obj._embedded['wp:featuredmedia']):
      return false;

    case isNotTypeOfUndefined(obj._embedded['wp:featuredmedia'][0].message):
      return false;

    case isNotTypeOfUndefined(
      obj._embedded['wp:featuredmedia'][0].media_details.sizes
    ):
      if (
        isNotTypeOfUndefined(
          obj._embedded['wp:featuredmedia'][0].media_details.sizes.medium_large
        )
      ) {
        return obj._embedded['wp:featuredmedia'][0].media_details.sizes
          .medium_large.source_url;
      }
      return obj._embedded['wp:featuredmedia'][0].media_details.sizes.full
        .source_url;

    default:
      return false;
  }
}

export function checkNested(obj /* , level1, level2, ... levelN */) {
  const args = Array.prototype.slice.call(arguments, 1);

  for (let i = 0; i < args.length; i++) {
    if (!obj || !obj.hasOwnProperty(args[i])) {
      return false;
    }
    obj = obj[args[i]];
  }
  return true;
}

export function isTypeOfUndefined(item) {
  return typeof item === 'undefined';
}

export function isNotTypeOfUndefined(item) {
  return typeof item !== 'undefined';
}

export function empty(item) {
  return !isNotTypeOfUndefined(item) || item === '' || item === null;
}

export function setDateString(str) {
  const date = new Date(str);
  let minutes = date.getMinutes();
  minutes = minutes < 10 ? `0${minutes}` : minutes;

  let hours = date.getHours();
  hours = hours < 10 ? `0${hours}` : hours;

  return `${date.getDay()}.${date.getMonth() +
    1}.${date.getFullYear()} ${hours}:${minutes}`;
  // return date.toUTCString();
}

export function getContentStringFromSlug(str) {
  if (isNotTypeOfUndefined(str)) {
    str = str.replace(/-/g, ' ');
    str = str
      .toLowerCase()
      .split(' ')
      .map(s => s.charAt(0).toUpperCase() + s.substring(1))
      .join(' ');
    return str;
  }
  return '';
}

export function hash(str) {
  return Array.from(str).reduce(
    (s, c) => (Math.imul(31, s) + c.charCodeAt(0)) | 0,
    0
  );
}

export function filterObject(obj) {
  const ret = {};
  Object.keys(obj)
    .filter(key => obj[key] !== undefined)
    .forEach(key => (ret[key] = obj[key]));
  return ret;
}
