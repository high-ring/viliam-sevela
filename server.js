/* @flow */

import fs from 'fs';
import path from 'path';
import logger from 'morgan';
import express from 'express';
import compression from 'compression';
import helmet from 'helmet';
import hpp from 'hpp';
import cors from 'cors';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { renderRoutes, matchRoutes } from 'react-router-config';
import { Provider } from 'react-redux';
import { ChunkExtractor, ChunkExtractorManager } from '@loadable/server';

import { HelmetProvider } from 'react-helmet-async';
import chalk from 'chalk';
import openBrowser from 'react-dev-utils/openBrowser';

import configureStore from './utils/configureStore';
import renderHtml from './utils/renderHtml';
import routes from '../app/routes';
import routers from '../api';
import config from '../config';
import { httpLogger, errorHandler } from './middlewares';
import pkg from '../../package';
import { filterObject, isNotTypeOfUndefined } from './utils/helpers';

const app = express();

// CORS
app.use(
  cors({
    origin: true,
    methods: 'GET,POST,OPTIONS',
    credentials: true
  })
);
app.options('*', cors());

// Logger middleware
app.use(httpLogger());

// Use helmet to secure Express with various HTTP headers
app.use(helmet());

// Prevent HTTP parameter pollution
app.use(hpp());
// Compress all requests
app.use(compression());

// non-www to www
if (!__DEV__) {
  app.use((req, res, next) => {
    if (req.headers.host.match(/^www/) !== null) {
      return res.redirect('http://' + req.headers.host.replace(/^www\./, '') + req.url);
    }

    next();
  });
}

// API
// app.use(`/api/${process.env.API_VERSION}`, proxy.proxyWeb);
app.use(`/api/v1`, routers);
app.use('/api/v1/*', (req, res) => {
  res.status(400).json('wrong_endpoint');
});

// Use for http request debug (show errors only)
app.use(logger('dev', { skip: (req, res) => res.statusCode < 400 }));

app.use(express.static(path.resolve(process.cwd(), 'public')));

if (__DEV__) {
  /* Run express as webpack dev server */
  const webpack = require('webpack');
  const webpackConfig = require('../../tools/webpack/config.babel');
  const compiler = webpack(webpackConfig);

  compiler.apply(new webpack.ProgressPlugin());

  app.use(
    require('webpack-dev-middleware')(compiler, {
      publicPath: webpackConfig.output.publicPath,
      headers: { 'Access-Control-Allow-Origin': '*' },
      hot: true,
      quiet: true, // Turn it on for friendly-errors-webpack-plugin
      noInfo: true,
      writeToDisk: true,
      stats: 'minimal',
      serverSideRender: true
    })
  );

  app.use(
    require('webpack-hot-middleware')(compiler, {
      log: false // Turn it off for friendly-errors-webpack-plugin
    })
  );
}

// PWA
app.use('/app-shell', (req, res) => {
  const statsFile = path.resolve(process.cwd(), 'public/loadable-stats.json');
  const extractor = new ChunkExtractor({ statsFile });

  res.status(200).send(renderHtml({}, {}, '', extractor, '', {}));
});

// Register server-side rendering middleware
app.get('*', (req, res) => {
  const { store } = configureStore({ url: req.url });

  // The method for loading data from server-side
  const loadBranchData = (): Promise<any> => {
    const branch = matchRoutes(routes, req.path);

    const promises = branch.map(({ route, match }) => {
      if (route.loadData) {
        return Promise.all(
          route
            .loadData({
              params: match.params,
              query: req.query,
              getState: store.getState,
              server: true
            })
            .map(item => store.dispatch(item))
        );
      }

      return Promise.resolve(null);
    });

    return Promise.all(promises);
  };

  (async () => {
    try {
      // Load data from server-side first
      await loadBranchData();

      const statsFile = path.resolve(
        process.cwd(),
        'public/loadable-stats.json'
      );
      const extractor = new ChunkExtractor({ statsFile });

      const staticContext = {};
      const helmetContext = {};

      const AppComponent = (
        <ChunkExtractorManager extractor={extractor}>
          <Provider store={store}>
            <HelmetProvider context={helmetContext}>
              {/* Setup React-Router server-side rendering */}
              <StaticRouter location={req.path} context={staticContext}>
                {renderRoutes(routes)}
              </StaticRouter>
            </HelmetProvider>
          </Provider>
        </ChunkExtractorManager>
      );

      const initialState = store.getState();
      const htmlContent = renderToString(AppComponent);
      const head = helmetContext.helmet;

      // Check if the render result contains a redirect, if so we need to set
      // the specific status and redirect header and end the response
      if (staticContext.url) {
        res.status(301).setHeader('Location', staticContext.url);
        res.end();

        return;
      }

      const defaultMeta = {
        DESCRIPTION: pkg.helmet.description,
        OG_URL: config.url.public + req.path,
        OG_TITLE: pkg.helmet.title,
        OG_DESCRIPTION: pkg.helmet.description,
        OG_IMAGE: './og.jpg',
        OG_IMAGE_HEIGHT: 1012,
        OG_IMAGE_WIDTH: 2440,
        OG_IMAGE_TYPE: 'image/jpeg'
      };
      let dynamicMeta = {};

      Object.keys(initialState).forEach(key => {
        const data = initialState[key];

        if (isNotTypeOfUndefined(data.meta)) {
          dynamicMeta = data.meta;
          return data.meta;
        }

        return false;
      });
      const meta = {
        ...filterObject(defaultMeta),
        ...filterObject(dynamicMeta)
      };

      // Check page status
      const status = staticContext.status === '404' ? 404 : 200;

      const faviconFile = path.resolve(process.cwd(), 'public/favicon.html');

      fs.readFile(faviconFile, 'utf8', function (err, data) {
        if (err) throw err;

        // Pass the route and initial state into html template
        res
          .status(status)
          .send(
            renderHtml(head, meta, data, extractor, htmlContent, initialState)
          );
      });
    } catch (err) {
      res.status(404).send('Not Found :(');

      console.error(chalk.red(`==> Rendering routes error: ${err}`));
    }
  })();
});

// mount error handler middleware last
app.use(errorHandler());

if (config.url.port) {
  app.listen(config.url.port, config.url.host, err => {
    const url = `${config.url.protocol}://${config.url.host}:${config.url.port}`;

    if (err) console.error(chalk.red(`==> Error: ${err}`));

    console.info(chalk.green(`==> Listening at ${url}`));

    // Open browser
    if (openBrowser(url))
      console.info(chalk.green("==> Opened on your browser's tab!"));
  });
} else {
  console.error(
    chalk.red('==> No PORT environment variable has been specified')
  );
}
