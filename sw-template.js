/*eslint-disable */
importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

if (workbox) {
  console.log(`Yay! Workbox is loaded 🎉`);
  workbox.setConfig({
    debug: false,
    skipWaiting: true,
    // cleanupOutdatedCaches: true,
  });

  // precahce and route asserts built by webpack
  workbox.precaching.precacheAndRoute([], {
    offlinePage: '/app-shell',
  }); // URLs to precache injected by workbox build
  // workbox.routing.registerRoute(new RegExp('.*.*'), new workbox.strategies.StaleWhileRevalidate());

  // active new service worker as long as it's installed
  workbox.core.clientsClaim();
  workbox.core.skipWaiting();

  // return app shell for all navigation requests
  // workbox.routing.registerNavigationRoute('/app-shell');
  workbox.routing.registerNavigationRoute(
    workbox.precaching.getCacheKeyForURL('/app-shell', {
      blacklist: [
        new RegExp('^/api/'), // Exclude URLs starting with /api/, as they're API calls (not navigation)
        new RegExp('/[^/]+\\.[^/]+$'), // Exclude URLs containing a dot, as they're likely a resource in public/ and not a SPA route
      ],
    })
  );

  workbox.core.setCacheNameDetails({
    prefix: 'nymandi-pwa',
    suffix: 'v1',
    precache: 'install-time',
    runtime: 'run-time',
    googleAnalytics: 'ga',
  });

  // Cache Google Fonts
  workbox.routing.registerRoute(
    "https://fonts.googleapis.com/(.*)",
    new workbox.strategies.CacheFirst({
      cacheName: "google-fonts",
      cacheableResponse: { statuses: [0, 200] }
    })
  );

  // Static content from Google
  workbox.routing.registerRoute(
    /.*(?:gstatic)\.com.*$/,
    new workbox.strategies.CacheFirst({
      cacheName: "google-static"
    })
  );

  // routing for api
  var apiNetworkFirst = new workbox.strategies.NetworkFirst({
    cacheName: 'nymandi-pwa-api-cache'
  });
  const apiCustomHandler = async (args) => {
    try {
      const response = await apiNetworkFirst.handle(args);
      return response || await caches.match(offlinePage);
    } catch (error) {
      return await caches.match(offlinePage);
    }
  };
  workbox.routing.registerRoute(
    /^(http|https):\/\/api\.nymandi\.cz\/(?:(?!_wpnonce=).)*$/i,
    apiCustomHandler
  );

  // routing for cloud served images
  workbox.routing.registerRoute(
    /^(http|https):\/\/.+\.(jpe?g|png|gif|svg|css)$/i,
    new workbox.strategies.CacheFirst({
      cacheName: 'nymandi-pwa-assets-cache',
      plugins: [
        new workbox.expiration.Plugin({
          // Only cache requests for a week
          maxAgeSeconds: 7 * 24 * 60 * 60,
          // Only cache 20 requests.
          maxEntries: 20
        }),
        new workbox.cacheableResponse.Plugin({
          statuses: [0, 200]
        })
      ]
    })
  );

  // routing for cloud served fonts
  workbox.routing.registerRoute(
    /^(http|https):\/\/.+\.(woff2)$/i,
    new workbox.strategies.CacheFirst({
      cacheName: 'nymandi-pwa-font-cache',
      plugins: [
        new workbox.expiration.Plugin({
          // Only cache requests for a week
          maxAgeSeconds: 7 * 24 * 60 * 60,
          // Only cache 20 requests.
          maxEntries: 20
        }),
        new workbox.cacheableResponse.Plugin({
          statuses: [0, 200]
        })
      ]
    })
  );

  // pages to cache (networkFirst)
  var offlinePage = '/app-shell';
  var pagesNetworkFirst = new workbox.strategies.NetworkFirst({
    cacheName: 'nymandi-cache-pages'
  });

  const pagesCustomHandler = async (args) => {
    try {
      const response = await pagesNetworkFirst.handle(args);
      return response || await caches.match(offlinePage);
    } catch (error) {
      return await caches.match(offlinePage);
    }
  };

  function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()/|[\]\\]/g, '\\$&'); // $& means the whole matched string
  }

  const escapedOrigin = escapeRegExp(location.origin + '/');

  // console.log('^' + escapedOrigin + '(\w*)');

  workbox.routing.registerRoute(
    new RegExp('^' + escapedOrigin + '(?!\\/assets)(.+)', 'i'),
    pagesCustomHandler
  );
} else {
  console.log(`Boo! Workbox didn't load 😬`);
}

/*eslint-enable */
